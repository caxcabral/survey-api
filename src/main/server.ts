import app from './config/app'

const PORT = process.env.PORT ?? 5050
app.listen(PORT, () => console.warn(`Server running @ http://localhost:${PORT}`))
