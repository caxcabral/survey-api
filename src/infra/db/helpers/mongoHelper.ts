import { MongoClient, Collection } from 'mongodb'

export const mongoHelper = {
  client: null as any,

  async connect (): Promise<void> {
    this.clientConfig = {
      useUnifiedTopology: true
    }
    if (!process.env.MONGO_URL) throw new Error('Mongo URI invalida')
    this.client = await MongoClient.connect(process.env.MONGO_URL, this.clientConfig)
  },

  async disconnect (): Promise<void> {
    if (this.client) {
      await this.client.close()
    }
  },

  getCollection (name: string): Collection {
    return this.client.db().collection(name)
  },

  map: (collection: any): any => {
    const { _id, ...collectionWithoutId } = collection
    return Object.assign({}, collectionWithoutId, { id: _id })
  }
}
