import { BcryptAdapter } from './bcryptAdapter'
import bcrypt from 'bcrypt'

jest.mock('bcrypt', () => ({
  async hash (password: string, salt: 12): Promise<string> {
    return new Promise(resolve => resolve('hashedPassword'))
  }
}))

const salt = 12
const makeSut = (): BcryptAdapter => {
  return new BcryptAdapter(salt)
}

describe('Bcrypt Adapter', () => {
  test('should call bcrypt with correct value', async () => {
    const sut = makeSut()
    const hashSpy = jest.spyOn(bcrypt, 'hash')
    await sut.encrypt('any_value')
    expect(hashSpy).toBeCalledWith('any_value', salt)
  })

  test('should throw if bcrypt throws', async () => {
    const sut = makeSut()
    jest.spyOn(bcrypt, 'hash').mockImplementationOnce(
      async () => Promise.reject(new Error('teste'))
    )
    const promise = sut.encrypt('any_value')
    await expect(promise).rejects.toThrowError()
  })

  test('should return a hash on success', async () => {
    const sut = makeSut()
    const hash = await sut.encrypt('any_value')
    expect(hash).toBe('hashedPassword')
  })
})
